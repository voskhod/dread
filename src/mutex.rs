use crate::futex;
use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut, Drop};
use std::sync::atomic::{AtomicU32, Ordering::*};

const UNLOCKED: u32 = 0;
const LOCKED_NO_WAITER: u32 = 1;
const LOCKED_MULTIPLE_WAITERS: u32 = 2;

pub struct Mutex<T> {
    state: AtomicU32,
    data: UnsafeCell<T>,
}

pub struct LockGuard<'a, T> {
    pub(crate) lock: &'a Mutex<T>,
}

impl<T> Mutex<T> {
    pub fn new(data: T) -> Mutex<T> {
        Mutex {
            state: UNLOCKED.into(),
            data: UnsafeCell::new(data),
        }
    }

    pub fn lock(&self) -> LockGuard<T> {
        if self
            .state
            .compare_exchange(UNLOCKED, LOCKED_NO_WAITER, Acquire, Relaxed)
            .is_err()
        {
            Self::lock_contended(&self.state);
        }

        LockGuard { lock: self }
    }

    fn lock_contended(state: &AtomicU32) {
        let mut spin_count = 0;

        while state.load(Relaxed) != 0 && spin_count < 100 {
            std::hint::spin_loop();
            spin_count += 1;
        }

        if state
            .compare_exchange(UNLOCKED, LOCKED_NO_WAITER, Acquire, Relaxed)
            .is_ok()
        {
            return;
        }

        while state.swap(LOCKED_MULTIPLE_WAITERS, Acquire) != UNLOCKED {
            // We are going to loop awyway, so we can ingore the "interruped".
            _ = futex::wait(state, LOCKED_MULTIPLE_WAITERS);
        }
    }
}

unsafe impl<T> Sync for Mutex<T> where T: Send {}

impl<'a, T> Deref for LockGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &*self.lock.data.get() }
    }
}

impl<'a, T> DerefMut for LockGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.lock.data.get() }
    }
}

impl<'a, T> Drop for LockGuard<'a, T> {
    fn drop(&mut self) {
        if self.lock.state.swap(UNLOCKED, Release) == LOCKED_MULTIPLE_WAITERS {
            futex::wake_one(&self.lock.state);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    // 1 second is far too long for miri...
    #[test]
    #[cfg(not(miri))]
    fn it_works() {
        let l = Mutex::new(0);

        thread::scope(|s| {
            let mut n = l.lock();
            s.spawn(|| {
                let mut n = l.lock();
                assert_eq!(*n, 1);
                *n += 1;
            });

            thread::sleep(std::time::Duration::new(1, 0));
            assert_eq!(*n, 0);
            *n += 1;
            drop(n);
        });
    }

    #[test]
    fn book() {
        let x = Mutex::new(Vec::new());
        thread::scope(|s| {
            s.spawn(|| x.lock().push(1));
            s.spawn(|| {
                let mut g = x.lock();
                g.push(2);
                g.push(2);
            });
        });
        let g = x.lock();
        assert!(g.as_slice() == [1, 2, 2] || g.as_slice() == [2, 2, 1]);
    }
}
