use std::cell::UnsafeCell;
use std::mem::ManuallyDrop;
use std::ops::Deref;
use std::ptr::NonNull;
use std::sync::atomic::{fence, AtomicUsize, Ordering};

struct ArcData<T> {
    data: UnsafeCell<ManuallyDrop<T>>,
    data_ref_count: AtomicUsize,
    // Number of weak + 1 if there is they are any arc.
    alloc_ref_count: AtomicUsize,
}

pub struct Arc<T> {
    shared: NonNull<ArcData<T>>,
}

pub struct Weak<T> {
    shared: NonNull<ArcData<T>>,
}

unsafe impl<T> Send for Arc<T> where T: Send + Sync {}
unsafe impl<T> Sync for Arc<T> where T: Send + Sync {}

unsafe impl<T> Send for Weak<T> where T: Send + Sync {}
unsafe impl<T> Sync for Weak<T> where T: Send + Sync {}

impl<T> Arc<T> {
    pub fn new(data: T) -> Self {
        let shared = ArcData {
            data: UnsafeCell::new(ManuallyDrop::new(data)),
            data_ref_count: 1.into(),
            alloc_ref_count: 1.into(),
        };
        Arc {
            shared: NonNull::from(Box::leak(Box::new(shared))),
        }
    }

    fn data(&self) -> &ArcData<T> {
        unsafe { self.shared.as_ref() }
    }

    //    pub fn get_mut(arc: &mut Self) -> Option<&mut T> {
    //        if arc
    //            .data()
    //            .alloc_ref_count
    //            .compare_exchange(1, usize::MAX, Ordering::Acquire, Ordering::Relaxed)
    //            .is_err()
    //        {
    //            return None;
    //        }
    //
    //        let is_unique = arc.data().data_ref_count.load(Ordering::Relaxed) == 1;
    //        arc.data().alloc_ref_count.store(1, Ordering::Release);
    //
    //        if !is_unique {
    //            return None;
    //        }
    //
    //        fence(Ordering::Acquire);
    //        unsafe { Some(&mut *arc.shared.as_mut().data.get()) }
    //    }

    pub fn downgrade(arc: &Self) -> Weak<T> {
        let mut n = arc.data().alloc_ref_count.load(Ordering::Relaxed);
        loop {
            if n == usize::MAX {
                std::hint::spin_loop();
                n = arc.data().alloc_ref_count.load(Ordering::Relaxed);
                continue;
            }
            assert!(n < usize::MAX);

            if let Err(e) = arc.data().alloc_ref_count.compare_exchange_weak(
                n,
                n + 1,
                Ordering::Acquire,
                Ordering::Relaxed,
            ) {
                n = e;
                continue;
            }

            return Weak { shared: arc.shared };
        }
    }
}

impl<T> Clone for Arc<T> {
    fn clone(&self) -> Self {
        let count = self.data().data_ref_count.fetch_add(1, Ordering::Relaxed);
        if count > usize::MAX / 2 {
            panic!("");
        }
        Arc {
            shared: self.shared,
        }
    }
}

impl<T> Deref for Arc<T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &*self.data().data.get() }
    }
}

impl<T> Drop for Arc<T> {
    fn drop(&mut self) {
        let count = self.data().data_ref_count.fetch_sub(1, Ordering::Release);
        if count == 1 {
            fence(Ordering::Acquire);
            unsafe {
                ManuallyDrop::drop(&mut *self.data().data.get());
            }
            drop(Weak {
                shared: self.shared,
            });
        }
    }
}

impl<T> Weak<T> {
    fn data(&self) -> &ArcData<T> {
        unsafe { self.shared.as_ref() }
    }

    pub fn upgrade(&self) -> Option<Arc<T>> {
        let mut n = self.data().data_ref_count.load(Ordering::Relaxed);
        loop {
            if n == 0 {
                return None;
            }

            assert!(n < usize::MAX);
            if let Err(e) = self.data().data_ref_count.compare_exchange_weak(
                n,
                n + 1,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                n = e;
                continue;
            }

            return Some(Arc {
                shared: self.shared,
            });
        }
    }
}

impl<T> Clone for Weak<T> {
    fn clone(&self) -> Self {
        let count = self.data().alloc_ref_count.fetch_add(1, Ordering::Relaxed);
        if count > usize::MAX / 2 {
            panic!("");
        }
        Weak {
            shared: self.shared,
        }
    }
}

impl<T> Drop for Weak<T> {
    fn drop(&mut self) {
        let count = self.data().alloc_ref_count.fetch_sub(1, Ordering::Release);
        if count == 1 {
            fence(Ordering::Acquire);
            unsafe {
                drop(Box::from_raw(self.shared.as_ptr()));
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    #[test]
    fn test() {
        static NUM_DROPS: AtomicUsize = AtomicUsize::new(0);

        struct DetectDrop;

        impl Drop for DetectDrop {
            fn drop(&mut self) {
                NUM_DROPS.fetch_add(1, Ordering::Relaxed);
            }
        }

        let x = Arc::new(("Hello", DetectDrop));
        let y = Arc::downgrade(&x);
        let z = Arc::downgrade(&x);

        let t = thread::spawn(move || {
            let y = y.upgrade().unwrap();
            assert_eq!(y.0, "Hello");
        });

        assert_eq!(x.0, "Hello");
        t.join().unwrap();

        assert_eq!(NUM_DROPS.load(Ordering::Relaxed), 0);
        assert!(z.upgrade().is_some());

        drop(x);

        assert_eq!(NUM_DROPS.load(Ordering::Relaxed), 1);
        assert!(z.upgrade().is_none());
    }
}
