use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut, Drop};
use std::sync::atomic::{AtomicBool, Ordering};

pub struct SpinLock<T> {
    locked: AtomicBool,
    data: UnsafeCell<T>,
}

pub struct LockGuard<'a, T> {
    lock: &'a SpinLock<T>,
}

impl<'a, T> Deref for LockGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &*self.lock.data.get() }
    }
}

impl<'a, T> DerefMut for LockGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.lock.data.get() }
    }
}

impl<'a, T> Drop for LockGuard<'a, T> {
    fn drop(&mut self) {
        self.lock.locked.store(false, Ordering::Release);
    }
}

impl<T> SpinLock<T> {
    pub fn new(data: T) -> Self {
        SpinLock {
            locked: AtomicBool::new(false),
            data: UnsafeCell::new(data),
        }
    }

    pub fn lock(&self) -> LockGuard<T> {
        while self
            .locked
            .compare_exchange_weak(false, true, Ordering::Acquire, Ordering::Relaxed)
            .is_err()
        {
            std::hint::spin_loop();
        }

        LockGuard { lock: self }
    }
}

unsafe impl<T> Sync for SpinLock<T> where T: Send {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    // 1 second is far too long for miri...
    #[test]
    #[cfg(not(miri))]
    fn it_works() {
        let l = SpinLock::new(0);

        thread::scope(|s| {
            let mut n = l.lock();
            s.spawn(|| {
                let mut n = l.lock();
                assert_eq!(*n, 1);
                *n += 1;
            });

            thread::sleep(std::time::Duration::new(1, 0));
            assert_eq!(*n, 0);
            *n += 1;
            drop(n);
        });
    }

    #[test]
    fn book() {
        let x = SpinLock::new(Vec::new());
        thread::scope(|s| {
            s.spawn(|| x.lock().push(1));
            s.spawn(|| {
                let mut g = x.lock();
                g.push(2);
                g.push(2);
            });
        });
        let g = x.lock();
        assert!(g.as_slice() == [1, 2, 2] || g.as_slice() == [2, 2, 1]);
    }
}
