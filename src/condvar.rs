


use crate::{futex, mutex::LockGuard};
use std::sync::atomic::{AtomicU32, AtomicUsize, Ordering::*};

pub struct CondVar {
    counter: AtomicU32,
    num_waiters: AtomicUsize,
}

impl CondVar {
    pub fn new() -> Self {
        Self { counter: 0.into(), num_waiters: 0.into() }
    }

    pub fn notify_one(&self) {
        if self.num_waiters.load(Relaxed) > 0 {
            self.counter.fetch_add(1, Relaxed);
            futex::wake_one(&self.counter);
        }
    }

    pub fn notify_all(&self) {
        if self.num_waiters.load(Relaxed) > 0 {
            self.counter.fetch_add(1, Relaxed);
            futex::wake_all(&self.counter);
        }
    }

    pub fn wait<'a, T>(&self, g: LockGuard<'a, T>) -> LockGuard<'a, T> {

        self.num_waiters.fetch_add(1, Relaxed);
        let counter_val = self.counter.load(Relaxed);
        let m = g.lock;
        drop(g);

        while futex::wait(&self.counter, counter_val).is_err() {}
        self.num_waiters.fetch_sub(1, Relaxed);

        m.lock()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;
    use crate::mutex::Mutex;
    use std::time::Duration;

    #[test]
    fn test() {

        let m = Mutex::new(0);
        let condvar = CondVar::new();

        let mut wakeups = 0;

        thread::scope(|s| {
            s.spawn(|| {
                thread::sleep(Duration::from_secs(1));
                *m.lock() = 123;
                condvar.notify_one();
            });

            let mut g = m.lock();
            while *g < 100 {
                g = condvar.wait(g);
                wakeups += 1;
            }

            assert_eq!(*g, 123);
        });

        assert!(wakeups < 10);
    }
}
