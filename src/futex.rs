#[cfg(not(target_os = "linux"))]
compile_error!("Linux only :(");

use std::sync::atomic::AtomicU32;

#[inline]
fn errno() -> libc::c_int {
    unsafe { *libc::__errno_location() }
}

pub struct Interrupted {}

pub fn wait(futex: &AtomicU32, expected: u32) -> Result<(), Interrupted> {
    let r = unsafe {
        libc::syscall(
            libc::SYS_futex,
            futex as *const AtomicU32,
            libc::FUTEX_WAIT | libc::FUTEX_PRIVATE_FLAG,
            expected,
            std::ptr::null::<libc::timespec>(),
        )
    };

    // Could also be a ETIMEDOUT but we have no timer set here.
    if r < 0 && errno() == libc::EINTR {
        return Err(Interrupted {});
    }

    Ok(())
}

pub fn wake_one(futex: &AtomicU32) {
    unsafe {
        libc::syscall(
            libc::SYS_futex,
            futex as *const AtomicU32,
            libc::FUTEX_WAKE | libc::FUTEX_PRIVATE_FLAG,
            1,
        )
    };
}

pub fn wake_all(futex: &AtomicU32) {
    unsafe {
        libc::syscall(
            libc::SYS_futex,
            futex as *const AtomicU32,
            libc::FUTEX_WAKE | libc::FUTEX_PRIVATE_FLAG,
            i32::MAX,
        )
    };
}
