use crate::futex;
use std::sync::atomic::{AtomicU32, Ordering::*};
use core::ops::{Deref, DerefMut, Drop};
use std::cell::UnsafeCell;

pub struct RWLock<T> {
    // Number of readers times 2, plus one if there is a writer waiting.
    // u32::MAX if write locked.
    state: AtomicU32,
    write_wake_counter: AtomicU32,
    data: UnsafeCell<T>,
}

pub struct ReadGuard<'a, T> {
    lock: &'a RWLock<T>,
}

pub struct WriteGuard<'a, T> {
    lock: &'a RWLock<T>,
}

unsafe impl<T> Sync for RWLock<T> where T: Send + Sync {}

impl<T> RWLock<T> {
    pub fn new(data: T) -> Self {
        Self { state: 0.into(), write_wake_counter: 0.into(), data: UnsafeCell::new(data) }
    }

    pub fn read(&self) -> ReadGuard<T> {
        let mut s = self.state.load(Relaxed);
        loop {
            if s % 2 == 0 {
                // Even: the writer can take the lock.
                assert!(s != u32::MAX - 2, "Too many readers !");
                match self.state.compare_exchange_weak(s, s + 2, Acquire, Relaxed) {
                    Ok(_) => return ReadGuard { lock: self },
                    Err(e) => s = e,
                }
            } else {
                // Odd: we wait for the writer to take the lock and releasing it.
                while futex::wait(&self.state, s).is_err() {}
                s = self.state.load(Relaxed);
            }
        }
    }

    pub fn write(&self) -> WriteGuard<T> {
        let mut s = self.state.load(Relaxed);
        loop {
            // There is no reader and the lock is not taken by a writer.
            if s <= 1 {
                match self.state.compare_exchange(s, u32::MAX, Acquire, Relaxed) {
                    Ok(_) => return WriteGuard { lock: self },
                    Err(e) => { s = e; continue; },
                }
            }
            
            // Some reader(s) have the lock, we block new readers (the state will be odd).
            if s % 2 == 0 {
                match self.state.compare_exchange(s, s + 1, Relaxed, Relaxed) {
                    Ok(_) => {},
                    Err(e) => { s = e; continue; },
                }
            }

            let w = self.write_wake_counter.load(Acquire);
            s = self.state.load(Relaxed);
            if s >= 2 {
                while futex::wait(&self.write_wake_counter, w).is_err() {}
                s = self.state.load(Relaxed);
            }
        }
    }
}

impl<'a, T> Deref for ReadGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &*self.lock.data.get() }
    }
}

impl<'a, T> Deref for WriteGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &*self.lock.data.get() }
    }
}

impl<'a, T> DerefMut for WriteGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.lock.data.get() }
    }
}

impl<'a, T> Drop for ReadGuard<'a, T> {
    fn drop(&mut self) {
        if self.lock.state.fetch_sub(2, Release) == 3 {
            self.lock.write_wake_counter.fetch_add(1, Release);
            futex::wake_one(&self.lock.write_wake_counter);
        }
    }
}

impl<'a, T> Drop for WriteGuard<'a, T> {
    fn drop(&mut self) {
        self.lock.state.store(0, Release);
        self.lock.write_wake_counter.fetch_add(1, Release);
        futex::wake_one(&self.lock.write_wake_counter);
        futex::wake_all(&self.lock.state);
    }
}
