use std::cell::UnsafeCell;
use std::marker::PhantomData;
use std::mem::MaybeUninit;
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread::Thread;

pub struct Sender<'a, T> {
    channel: &'a Channel<T>,
    reciving_thread: Thread,
}

impl<T> Sender<'_, T> {
    pub fn send(self, msg: T) {
        unsafe {
            (*self.channel.data.get()).write(msg);
        }
        self.channel.ready.store(true, Ordering::Release);
        self.reciving_thread.unpark();
    }
}

impl<T> Receiver<'_, T> {
    pub fn is_ready(&self) -> bool {
        self.channel.ready.load(Ordering::Relaxed)
    }

    pub fn receive(self) -> T {
        while !self.channel.ready.swap(false, Ordering::Acquire) {
            std::thread::park();
        }
        unsafe { (*self.channel.data.get()).assume_init_read() }
    }
}

pub struct Receiver<'a, T> {
    channel: &'a Channel<T>,
    _no_send: PhantomData<*const ()>,
}

pub struct Channel<T> {
    data: UnsafeCell<MaybeUninit<T>>,
    ready: AtomicBool,
}

impl<T> Channel<T> {
    pub fn new() -> Self {
        Channel {
            data: UnsafeCell::new(MaybeUninit::uninit()),
            ready: AtomicBool::new(false),
        }
    }

    pub fn split(&mut self) -> (Sender<T>, Receiver<T>) {
        *self = Self::new();
        (
            Sender {
                channel: self,
                reciving_thread: std::thread::current(),
            },
            Receiver {
                channel: self,
                _no_send: PhantomData,
            },
        )
    }
}

impl<T> Default for Channel<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Drop for Channel<T> {
    fn drop(&mut self) {
        unsafe {
            self.data.get_mut().assume_init_drop();
        }
    }
}

unsafe impl<T> Sync for Channel<T> where T: Send {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    #[test]
    fn book() {
        let mut channel = Channel::new();

        thread::scope(|s| {
            let (sender, receiver) = channel.split();
            s.spawn(|| {
                sender.send("Hello");
                thread::current().unpark();
            });

            assert_eq!(receiver.receive(), "Hello");
        });
    }
}
